# Abyss - survivors roguelike

Abyss is a top down survivors roguelike.

## Platforms

Abyss is being developed for both desktop and mobile.

## Links

- [codeberg repository](https://codeberg.org/paperweights/abyss)
