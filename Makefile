.POSIX:

SRV_DIR = /srv/www/paperweights
LDFLAGS = -static -s

CC := cc
CFLAGS := -std=c99 -pedantic -Wall -Wextra
CPPFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=700

all: html

html: build-page htmlclean
	find * -type d -exec sh -ec 'mkdir -p ${SRV_DIR}/$$0; ./build-page "$$0" > ${SRV_DIR}/$$0/index.html' {} \;

htmlclean:
	-find * -type f -name \*.html -exec rm {} \;

build-page: build-page.c

clean:
	rm -f build-page

.PHONY: all html htmlclean clean
